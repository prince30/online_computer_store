package com.computer.store.online_computer_store_backend.dao;

import com.computer.store.online_computer_store_backend.dto.Product;
import java.util.List;

/**
 *
 * @author PRINCE
 */
public interface ProductDAO {

    Product get(int id);

    List<Product> list();

    boolean add(Product product);

    boolean update(Product product);

    boolean delete(Product product);

    //BUSINESS METHODS
    List<Product> listActiveProducts();

    List<Product> listActiveProductsByCategory(int categoryId);

    List<Product> getLatestActiveProducts(int count);

}
