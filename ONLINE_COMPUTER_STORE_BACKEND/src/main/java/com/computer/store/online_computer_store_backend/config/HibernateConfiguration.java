package com.computer.store.online_computer_store_backend.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author PRINCE
 */
@Configuration
@EnableTransactionManagement
public class HibernateConfiguration {

    /*
    H2 DATABASE ENGINE CONFIGURATION
     */
    private final static String DATABASE_URL = "jdbc:h2:tcp://localhost/~/online_computer_store";
    private final static String DATABASE_DRIVER = "org.h2.Driver";
    private final static String DATABASE_DIALECT = "org.hibernate.dialect.H2Dialect";
    private final static String DATABASE_USERNAME = "sa";
    private final static String DATABASE_PASSWORD = "";

    /*
    MYSQL DATABASE CONFIGURATION
     */
//    private final static String DATABASE_URL = "jdbc:mysql://localhost:3306/computer_store";
//    private final static String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
//    private final static String DATABASE_DIALECT = "org.hibernate.dialect.MySQLDialect";
//    private final static String DATABASE_USERNAME = "root";
//    private final static String DATABASE_PASSWORD = "";
    //DATASOURCE BEAN WILL BE AVAILABLE
    @Bean
    public DataSource getDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();

        basicDataSource.setDriverClassName(DATABASE_DRIVER);
        basicDataSource.setUrl(DATABASE_URL);
        basicDataSource.setUsername(DATABASE_USERNAME);
        basicDataSource.setPassword(DATABASE_PASSWORD);

        return basicDataSource;
    }

    //SESSIONFACTORY BEAN WILL BE AVAILABLE
    @Bean
    public SessionFactory getSessionFactory(DataSource dataSource) {

        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource);
        builder.addProperties(getHibernateProperties());
        builder.scanPackages("com.computer.store.online_computer_store_backend.dto");
        return builder.buildSessionFactory();
    }

//    ALL THE HIBERNATE PROPERTIES RETURN IN THE METHOD
    private Properties getHibernateProperties() {
        Properties properties = new Properties();

        properties.put("hibernate.dialect", DATABASE_DIALECT);
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.validator.apply_to_ddl", "true");
        properties.put("hibernate.connection.autocommit", "true");

        return properties;

    }

//    TRANSACTIONMANAGER BEAN
    @Bean
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }
}
