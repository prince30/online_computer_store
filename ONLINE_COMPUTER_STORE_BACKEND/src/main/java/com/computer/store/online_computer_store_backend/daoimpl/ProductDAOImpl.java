package com.computer.store.online_computer_store_backend.daoimpl;

import com.computer.store.online_computer_store_backend.dao.ProductDAO;
import com.computer.store.online_computer_store_backend.dto.Product;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author PRINCE
 */
@Repository("productDAO")
@Transactional
public class ProductDAOImpl implements ProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     *
     * @param productId
     * @return single product
     */
    @Override
    public Product get(int productId) {
        try {
            return sessionFactory
                    .getCurrentSession()
                    .get(Product.class, Integer.valueOf(productId));
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;

    }

    /**
     *
     * @return product list
     */
    @Override
    public List<Product> list() {
        return sessionFactory
                .getCurrentSession()
                .createQuery("FROM Product", Product.class)
                .getResultList();
    }

    /**
     *
     * @param product
     * @return add product in DB
     */
    @Override
    public boolean add(Product product) {
        try {
            //ADD TO THE PRODUCT TO THE DATABASE
            sessionFactory.getCurrentSession().persist(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param product
     * @return update product list
     */
    @Override
    public boolean update(Product product) {
        try {
            sessionFactory.getCurrentSession().update(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *
     * @param product
     * @return active product list
     */
    @Override
    public boolean delete(Product product) {
        try {
            product.setActive(false);
            sessionFactory.getCurrentSession().update(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Product> listActiveProducts() {
        String selectActiveProduct = "FROM Product WHERE active=:active";

        return sessionFactory
                .getCurrentSession()
                .createQuery(selectActiveProduct, Product.class)
                .setParameter("active", true)
                .getResultList();
    }

    @Override
    public List<Product> listActiveProductsByCategory(int categoryId) {
        String selectActiveProductsByCategory = "FROM Product WHERE active=:active AND categoryId=:categoryId";

        return sessionFactory
                .getCurrentSession()
                .createQuery(selectActiveProductsByCategory, Product.class)
                .setParameter("active", true)
                .setParameter("categoryId", categoryId)
                .getResultList();
    }

    @Override
    public List<Product> getLatestActiveProducts(int count) {
        return sessionFactory
                .getCurrentSession()
                .createQuery("FROM Product WHERE active=:active ORDER BY id", Product.class)
                .setParameter("active", true)
                .setFirstResult(0)
                .setMaxResults(count)
                .getResultList();
    }

}
