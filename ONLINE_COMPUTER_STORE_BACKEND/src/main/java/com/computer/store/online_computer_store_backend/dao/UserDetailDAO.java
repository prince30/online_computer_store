package com.computer.store.online_computer_store_backend.dao;

import com.computer.store.online_computer_store_backend.dto.UserDetail;

/**
 *
 * @author PRINCE
 */
public interface UserDetailDAO {
    boolean add(UserDetail userDetail);
}
