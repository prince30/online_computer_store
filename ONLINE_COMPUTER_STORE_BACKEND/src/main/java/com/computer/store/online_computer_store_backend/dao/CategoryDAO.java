package com.computer.store.online_computer_store_backend.dao;

import com.computer.store.online_computer_store_backend.dto.Category;
import java.util.List;

/**
 *
 * @author PRINCE
 */
public interface CategoryDAO {

    Category get(int id);

    List<Category> list();

    boolean add(Category category);

    boolean update(Category category);

    boolean delete(Category category);
}
