package com.computer.store.online_computer_store_backend.daoimpl;

import com.computer.store.online_computer_store_backend.dao.UserDetailDAO;
import com.computer.store.online_computer_store_backend.dto.UserDetail;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author PRINCE
 */
@Repository("userDetailDAO")
@Transactional
public class UserDetailDAOImpl implements UserDetailDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean add(UserDetail userDetail) {
        try {
            //ADD TO THE CATEGORY TO THE DATABASE
            sessionFactory.getCurrentSession().persist(userDetail);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
