package com.computer.store.online_computer_store_backend.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author PRINCE
 */
@Entity
@Table(name = "products")
public class Product {

    /**
     * private fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String code;
    @NotBlank(message = "Please enter the product name!")
    private String name;
    @NotBlank(message = "Please enter the brand name!")
    private String brand;
    @Type(type = "text")
    @JsonIgnore
    @NotBlank(message = "Please enter the product description!")
    private String description;
    @Min(value = 1,message = "The price can not be less than 1")
    @Column(name = "unit_price")
    private double unitPrice;
    private int quantity;
    @Column(name = "is_active")
    private boolean active = true;
    @Column(name = "category_id")
    @JsonIgnore
    private int categoryId;
    @Column(name = "supplier_id")
    @JsonIgnore
    private int supplierId;
    private int purchase;
    private int views;
    
    @Transient
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
    
    

    //DEFAULT CONTRUCTOR
    public Product() {
        this.code = "PRD" + UUID.randomUUID().toString().substring(26).toUpperCase();
    }

    //GETTER AND SETTER
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getPurchase() {
        return purchase;
    }

    public void setPurchase(int purchase) {
        this.purchase = purchase;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", code=" + code + ", name=" + name + ", brand=" + brand + ", description=" + description + ", unitPrice=" + unitPrice + ", quantity=" + quantity + ", active=" + active + ", categoryId=" + categoryId + ", supplierId=" + supplierId + ", purchase=" + purchase + ", views=" + views + '}';
    }

}
