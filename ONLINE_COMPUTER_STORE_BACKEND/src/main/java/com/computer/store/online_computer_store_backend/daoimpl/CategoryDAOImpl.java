package com.computer.store.online_computer_store_backend.daoimpl;

import com.computer.store.online_computer_store_backend.dao.CategoryDAO;
import com.computer.store.online_computer_store_backend.dto.Category;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("categoryDAO")
@Transactional
public class CategoryDAOImpl implements CategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;
//    private static List<Category> categories = new ArrayList<>();
//
//    static {
//        Category category = new Category();
//
//        category.setId(1);
//        category.setName("Laptop");
//        category.setDescription("Good.....");
//        category.setImageUrl("cat.jpg");
//
//        categories.add(category);
//
//        category = new Category();
//        category.setId(2);
//        category.setName("PC");
//        category.setDescription("Good.....");
//        category.setImageUrl("cat.jpg");
//
//        categories.add(category);
//
//        category = new Category();
//        category.setId(3);
//        category.setName("keyboard");
//        category.setDescription("Good.....");
//        category.setImageUrl("cat.jpg");
//
//        categories.add(category);
//    }

    @Override
    public List<Category> list() {
        String selectActiveCategory = "FROM Category WHERE active=:active";

        Query query = sessionFactory.getCurrentSession().createQuery(selectActiveCategory);

        query.setParameter("active", true);

        return query.getResultList();
    }

    /*
    GETTING SINGLE CATEGORY BASED ON ID
     */
    @Override
    public Category get(int id) {
//        for (Category category : categories) {
//            if (category.getId() == id) {
//                return category;
//            }
//        }
//        return null;
        //return sessionFactory.getCurrentSession().get(Category.class, Integer.valueOf(id));

        return sessionFactory.getCurrentSession().get(Category.class, Integer.valueOf(id));
    }

    @Override
    public boolean add(Category category) {
        try {
            //ADD TO THE CATEGORY TO THE DATABASE
            sessionFactory.getCurrentSession().persist(category);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean update(Category category) {
        try {
            //ADD TO THE CATEGORY TO THE DATABASE
            sessionFactory.getCurrentSession().update(category);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Category category) {
        category.setActive(false);
        try {
            //ADD TO THE CATEGORY TO THE DATABASE
            sessionFactory.getCurrentSession().update(category);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
