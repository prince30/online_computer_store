$(function () {
    switch (menu) {
        case 'About Us':
            $('#about').addClass('active');
            break;
        case 'Contact Us':
            $('#contact').addClass('active');
            break;
        case 'All Products':
            $('#listProducts').addClass('active');
            break;
        case 'Manage Products':
            $('#manageProducts').addClass('active');
            break;
        default:
            if (menu == 'Home')
                break;
            $('#listProducts').addClass('active');
            $('#a_' + menu).addClass('active');
            break;
    }

//code for data FOR jQuery tables

//    var products = [
//        ['1', 'Mobi'],
//        ['2', 'GGOD'],
//        ['3', 'CAT'],
//        ['4', 'LAPTOP'],
//        ['5', 'TV'],
//    ];
//--------------------------------------------------------------------------------------------------------------------------------
    var $table = $('#productListTable');
    if ($table.length) {

        var jsonURL = '';
        if (window.categoryId == '') {
            jsonURL = window.contextRoot + '/json/data/all/products';
        } else {
            jsonURL = window.contextRoot + '/json/data/category/' + window.categoryId + '/products';
        }

        $table.DataTable({
            lengthMenu: [[3, 5, 10, -1], ['3 Recodes', '5 Recodes', '10 Recodes', 'All']],
            pageLength: 5,
//            data: products,
            ajax: {
                url: jsonURL,
                dataSrc: ''
            },
            columns: [
                {
                    data: 'code',
                    mRender: function (data, type, row) {
                        return  '<img src="' + window.contextRoot + '/resources/images/' + data + '.jpg" class="dataTableImg"/>';
                    }
                },
                {
                    data: 'name'
                },
                {
                    data: 'brand'
                },
                {
                    data: 'unitPrice',
                    mRender: function (data, type, row) {
                        return '&#8377; ' + data
                    }
                },
                {
                    data: 'quantity',
                    mRender: function (data, type, row) {
                        if (data < 1) {
                            return '<span style="color: red">Out of stock!</span>';
                        }
                        return data;
                    }
                },
                {
                    data: 'id',
                    mRender: function (data, type, row) {
                        var str = '';
                        str += '<a href="' + window.contextRoot + '/show/' + data + '/product" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span>View</a> &#160;';
                        if (row.quantity < 1) {
                            str += '<a href="javascript:void(0)" class="btn btn-success disabled"><span class="glyphicon glyphicon-shopping-cart"></span>Add to Cart</a>';
                        } else {
                            str += '<a href="' + window.contextRoot + '/cart/add/' + data + '/product"class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart"></span>Add to Cart</a>';
                        }

                        return str;
                    }
                }
            ]
        });
    }


//------------------------------------------------------------------------------------------------
//dismissing the alert after 3 seconds
    var $alert = $('.alert');
    if ($alert.length) {
        setTimeout(function () {
            $alert.fadeOut('slow');
        }, 3000)
    }
//--------------------------------------------------------------------------------

//    $('.switch input[type="checkbox"]').on('change', function () {
//        var checkbox = $(this);
//        var checked = checkbox.prop('checked');
//        var dmsg = (checked) ? 'you want the activate the product?' :
//                'you want the deactivate the product?';
//        var value = checkbox.prop('value');
//        bootbox.confirm({
//            size: 'medium',
//            title: 'Product Activation & Deactivation',
//            message: dmsg,
//            callback: function (confirmed) {
//                if (confirmed) {
//                    console.log(value);
//                    bootbox.alert({
//                        size: 'medium',
//                        title: 'Information',
//                        message: 'you are going to perform operation on product' + value
//                    });
//                } else {
//                    checkbox.prop('checked', !checked);
//                }
//            }
//        });
//
//    });



//---------------------------------------------------------------------------------------

    var $adminProductsTable = $('#adminProductsTable');
    if ($adminProductsTable.length) {

        var jsonUrl = window.contextRoot + '/json/data/admin/all/products';
        $adminProductsTable.DataTable({
            lengthMenu: [[10, 30, 50, -1], ['10 Recodes', '30 Recodes', '50 Recodes', 'All']],
            pageLength: 30,
            ajax: {
                url: jsonUrl,
                dataSrc: ''
            },
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'code',
                    mRender: function (data, type, row) {
                        return  '<img src="' + window.contextRoot + '/resources/images/' + data + '.jpg" class="adminTableImg"/>';
                    }
                },
                {
                    data: 'name'
                },
                {
                    data: 'brand'
                },
                {
                    data: 'quantity',
                    mRender: function (data, type, row) {
                        if (data < 1) {
                            return '<span style="color: red">Out of stock!</span>';
                        }
                        return data;
                    }
                },
                {
                    data: 'unitPrice',
                    mRender: function (data, type, row) {
                        return '&#8377; ' + data
                    }
                },
                {
                    data: 'active',
                    bSortable: false,
                    mRender: function (data, type, row) {

                        var str = '';
                        str += '<label class="switch">';
                        if (data) {
                            str += '<input type="checkbox" checked="checked" value="' + row.id + '"/>';
                        } else {
                            str += '<input type="checkbox" value="' + row.id + '"/>';
                        }
                        str += '<div class="slider"></div></div>';
                        return str;
                    }
                }
                ,
                {
                    data: 'id',
                    bSortable: false,
                    mRender: function (data, type, row) {
                        var str = '';
                        str += '<a href="' + window.contextRoot + '/manage/' + data + '/product" class="btn btn-warning">';
                        str += '<span class="glyphicon glyphicon-pencil"></span>';
                        return str;
                    }
                }
            ],
            initComplete: function () {
                var api = this.api();
                api.$('.switch input[type="checkbox"]').on('change', function () {
                    var checkbox = $(this);
                    var checked = checkbox.prop('checked');
                    var dmsg = (checked) ? 'you want the activate the product?' :
                            'you want the deactivate the product?';
                    var value = checkbox.prop('value');
                    bootbox.confirm({
                        size: 'medium',
                        title: 'Product Activation & Deactivation',
                        message: dmsg,
                        callback: function (confirmed) {
                            if (confirmed) {
                                console.log(value);
                                var activationUrl = window.contextRoot + '/manage/product/' + value + '/activation';
                                $.post(activationUrl, function (data) {
                                    bootbox.alert({
                                        size: 'medium',
                                        title: 'Information',
                                        message: 'you are going to perform operation on product' + value
                                    });
                                });
                            } else {
                                checkbox.prop('checked', !checked);
                            }
                        }
                    });
                });
            }

        });
    }





}
);


