package com.computer.store.online_computer_store.validator;

import com.computer.store.online_computer_store_backend.dto.Product;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author PRINCE
 */
public class ProductValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validate(Object target, Errors errors) {

        Product product = (Product) target;

        // whether file is selected or not
        if (product.getFile() == null || product.getFile().getOriginalFilename().equals("")) {
            errors.rejectValue("file", null, "Please select an image file");
            return;
        }

        if (product.getFile().getContentType().equals("images/jpeg")
                || product.getFile().getContentType().equals("images/png")
                || product.getFile().getContentType().equals("images/gif")) {
            errors.rejectValue("file", null, "Please select only  an image file");
        }

    }

}
