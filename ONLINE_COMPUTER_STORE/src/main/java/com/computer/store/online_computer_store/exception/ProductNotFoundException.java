package com.computer.store.online_computer_store.exception;

import java.io.Serializable;

/**
 *
 * @author PRINCE
 */
public class ProductNotFoundException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;
    private String mesasge;

    public ProductNotFoundException() {
        this("Product is not available");
    }

    public ProductNotFoundException(String message) {
        this.mesasge = System.currentTimeMillis() + ": " + message;
    }

    public String getMesasge() {
        return mesasge;
    }
}
