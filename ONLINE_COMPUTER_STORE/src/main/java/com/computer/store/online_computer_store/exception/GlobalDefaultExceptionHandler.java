package com.computer.store.online_computer_store.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 *
 * @author PRINCE
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handlerNoHandlerFoundException() {
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("errorTitle", "The page is not constructed");
        mv.addObject("errorDescription", "The page  you are looking, not available now!");
        mv.addObject("title", "404 error page");
        return mv;

    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ModelAndView handlerProductNotFoundException() {
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("errorTitle", "Product is not available");
        mv.addObject("errorDescription", "The product  you are looking, not available now!");
        mv.addObject("title", "Product is unavailable");
        return mv;

    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handlerException(Exception ex) {
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("errorTitle", "Contact your administrator!!");

        /*
        only for debugging my application
         */
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        mv.addObject("errorDescription", sw.toString());
        mv.addObject("title", "Error");
        return mv;

    }
}
