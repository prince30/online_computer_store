<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<div class="container">
    <div class="row">
        <c:if test="${not empty message}">
            <div class="col-xs-12">
                <div class="alert alert-success alert-dismissible">
                    <button  type="button" class="close" data-dismiss="alert">&times;</button>
                    ${message}
                </div>
            </div>
        </c:if>


        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="text-center">Product Management</h4>
                </div>
                <div class="panel-body">
                    <sf:form  class="form-horizontal" modelAttribute="product" action="${contextRoot}/manage/products" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Product Name</label>
                            <div class="col-sm-9">
                                <sf:input type="text" class="form-control" id="inputEmail3" path="name" placeholder="Enter Name"/>
                                <sf:errors path="name" cssClass="help-block" element="em" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Brand Name</label>
                            <div class="col-sm-9">
                                <sf:input type="text" class="form-control" id="inputEmail3" path="brand" placeholder="Enter Brand Name"/>
                                <sf:errors path="brand" cssClass="help-block" element="em" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Product Description</label>
                            <div class="col-sm-9">
                                <sf:textarea class="form-control" path="description" rows="3" placeholder="Enter Product Description"/>
                                <sf:errors path="description" cssClass="help-block" element="em" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Unit Price</label>
                            <div class="col-sm-9">
                                <sf:input type="text" class="form-control" id="inputPassword3" path="unitPrice" placeholder="Enter Unit Price"/>
                                <sf:errors path="unitPrice" cssClass="help-block" element="em" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Quantity</label>
                            <div class="col-sm-9">
                                <sf:input type="text" class="form-control" id="inputPassword3" path="quantity" placeholder="Enter Quantity"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Select an Image</label>
                            <div class="col-sm-9">
                                <sf:input type="file" class="form-control" id="inputPassword3" path="file" />
                                <sf:errors path="file" cssClass="help-block" element="em" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Category</label>
                            <div class="col-sm-5">
                                <sf:select class="form-control" path="categoryId"
                                           items="${categories}"
                                           itemLabel="name"
                                           itemValue="id"
                                           />

                            </div>
                            <c:if test="${product.id==0}">
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-warning pull-right" data-toggle="modal" data-target="#categoryModal">Add Category</button>
                                </div>
                            </c:if>

                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-info pull-right">Submit</button>
                                <sf:hidden  path="id"/>
                                <sf:hidden  path="active"/>
                                <sf:hidden  path="code"/>
                                <sf:hidden  path="purchase"/>
                                <sf:hidden  path="supplierId"/>
                                <sf:hidden  path="views"/>
                            </div>
                        </div>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Available Products</h3>
            <hr/>
        </div>
        <div class="col-xs-12">
            <div style="overflow: auto">
                <table id="adminProductsTable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>&#160;</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Active</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>&#160;</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Active</th>
                            <th>Edit</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Add New Category</h4>
                </div>
                <div class="modal-body">
                    <sf:form   modelAttribute="category" action="${contextRoot}/manage/category" method="POST">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Name:</label>
                            <sf:input type="text" class="form-control" id="inputEmail3" path="name"/>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Description:</label>
                            <sf:textarea class="form-control" path="description" rows="3" />
                        </div>     

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
</div>
